from __future__ import print_function

import os
import sys

sys.setrecursionlimit(10000)

from keras import backend as K, models
from keras.layers import Conv2D, MaxPooling2D, UpSampling2D
from keras.layers.core import Activation
from keras.layers.normalization import BatchNormalization
from keras.optimizers import Adam

K.set_image_data_format('channels_last')  # TF dimension ordering in this code

# img_rows = 96
# img_cols = 128
#
# smooth = 1.
# epochs = 200
#
#
# def dice_coef(y_true, y_pred):
#     y_true_f = K.flatten(y_true)
#     y_pred_f = K.flatten(y_pred)
#     intersection = K.sum(y_true_f * y_pred_f)
#     return (2. * intersection + smooth) / (K.sum(y_true_f) + K.sum(y_pred_f) + smooth)
#
#
# def dice_coef_loss(y_true, y_pred):
#     return -dice_coef(y_true, y_pred)
#
#
# def precision(y_true, y_pred):
#     """Precision metric.
#
#     Only computes a batch-wise average of precision.
#
#     Computes the precision, a metric for multi-label classification of
#     how many selected items are relevant.
#     """
#     true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
#     predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
#     precision = true_positives / (predicted_positives + K.epsilon())
#     return precision
#
#
# def recall(y_true, y_pred):
#     """Recall metric.
#
#     Only computes a batch-wise average of recall.
#
#     Computes the recall, a metric for multi-label classification of
#     how many relevant items are selected.
#     """
#     true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
#     possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
#     recall = true_positives / (possible_positives + K.epsilon())
#     return recall
#
#
# def f1score(y_true, y_pred):
#     def recall(y_true, y_pred):
#         """Recall metric.
#
#         Only computes a batch-wise average of recall.
#
#         Computes the recall, a metric for multi-label classification of
#         how many relevant items are selected.
#         """
#         true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
#         possible_positives = K.sum(K.round(K.clip(y_true, 0, 1)))
#         recall = true_positives / (possible_positives + K.epsilon())
#         return recall
#
#     def precision(y_true, y_pred):
#         """Precision metric.
#
#         Only computes a batch-wise average of precision.
#
#         Computes the precision, a metric for multi-label classification of
#         how many selected items are relevant.
#         """
#         true_positives = K.sum(K.round(K.clip(y_true * y_pred, 0, 1)))
#         predicted_positives = K.sum(K.round(K.clip(y_pred, 0, 1)))
#         precision = true_positives / (predicted_positives + K.epsilon())
#         return precision
#
#     precision = precision(y_true, y_pred)
#     recall = recall(y_true, y_pred)
#     return 2 * ((precision * recall) / (precision + recall))

def get_segnet(img_rows, img_cols):
    kernel = 3
    base = 16
    act_function = 'relu'

    encoding_layers = [
        Conv2D(base, (kernel, kernel), padding='same', input_shape=(img_rows, img_cols, 1)),
        BatchNormalization(axis=3),
        Activation(act_function),
        Conv2D(base, (kernel, kernel), padding='same'),
        BatchNormalization(axis=3),
        Activation(act_function),
        MaxPooling2D(),
        # Dropout(0.5),

        Conv2D(2*base, (kernel, kernel), padding='same'),
        BatchNormalization(axis=3),
        Activation(act_function),
        Conv2D(2*base, (kernel, kernel), padding='same'),
        BatchNormalization(axis=3),
        Activation(act_function),
        MaxPooling2D(),
        # Dropout(0.5),

        Conv2D(4*base, (kernel, kernel), padding='same'),
        BatchNormalization(axis=3),
        Activation(act_function),
        Conv2D(4*base, (kernel, kernel), padding='same'),
        BatchNormalization(axis=3),
        Activation(act_function),
        Conv2D(4*base, (kernel, kernel), padding='same'),
        BatchNormalization(axis=3),
        Activation(act_function),
        MaxPooling2D(),
        # Dropout(0.5),

        Conv2D(8*base, (kernel, kernel), padding='same'),
        BatchNormalization(axis=3),
        Activation(act_function),
        Conv2D(8*base, (kernel, kernel), padding='same'),
        BatchNormalization(axis=3),
        Activation(act_function),
        Conv2D(8*base, (kernel, kernel), padding='same'),
        BatchNormalization(axis=3),
        Activation(act_function),
        MaxPooling2D(),
        # Dropout(0.5),

        Conv2D(8*base, (kernel, kernel), padding='same'),
        BatchNormalization(axis=3),
        Activation(act_function),
        Conv2D(8*base, (kernel, kernel), padding='same'),
        BatchNormalization(axis=3),
        Activation(act_function),
        Conv2D(8*base, (kernel, kernel), padding='same'),
        BatchNormalization(axis=3),
        Activation(act_function),
        MaxPooling2D(),
        # Dropout(0.5),
    ]

    autoencoder = models.Sequential()
    autoencoder.encoding_layers = encoding_layers

    for l in autoencoder.encoding_layers:
        autoencoder.add(l)

    decoding_layers = [
        UpSampling2D(size=(2, 2)),
        Conv2D(8*base, (kernel, kernel), padding='same'),
        BatchNormalization(axis=3),
        Activation(act_function),
        Conv2D(8*base, (kernel, kernel), padding='same'),
        BatchNormalization(axis=3),
        Activation(act_function),
        Conv2D(8*base, (kernel, kernel), padding='same'),
        BatchNormalization(axis=3),
        Activation(act_function),
        # Dropout(0.5),

        UpSampling2D(size=(2, 2)),
        Conv2D(8*base, (kernel, kernel), padding='same'),
        BatchNormalization(axis=3),
        Activation(act_function),
        Conv2D(8*base, (kernel, kernel), padding='same'),
        BatchNormalization(axis=3),
        Activation(act_function),
        Conv2D(8*base, (kernel, kernel), padding='same'),
        BatchNormalization(axis=3),
        Activation(act_function),
        # Dropout(0.5),

        UpSampling2D(size=(2, 2)),
        Conv2D(4*base, (kernel, kernel), padding='same'),
        BatchNormalization(axis=3),
        Activation(act_function),
        Conv2D(4*base, (kernel, kernel), padding='same'),
        BatchNormalization(axis=3),
        Activation(act_function),
        Conv2D(2*base, (kernel, kernel), padding='same'),
        BatchNormalization(),
        Activation(act_function),
        # Dropout(0.5),

        UpSampling2D(size=(2, 2)),
        Conv2D(2*base, (kernel, kernel), padding='same'),
        BatchNormalization(axis=3),
        Activation(act_function),
        Conv2D(base, (kernel, kernel), padding='same'),
        BatchNormalization(axis=3),
        Activation(act_function),
        # Dropout(0.5),

        UpSampling2D(size=(2, 2)),
        Conv2D(base, (kernel, kernel), padding='same'),
        BatchNormalization(axis=3),
        Activation(act_function),
        # Dropout(0.5),

        Conv2D(1, (1, 1), padding='valid'),
        BatchNormalization(axis=3),
    ]
    autoencoder.decoding_layers = decoding_layers
    for l in autoencoder.decoding_layers:
        autoencoder.add(l)

    autoencoder.add(Activation('sigmoid'))
    # autoencoder.compile(loss=dice_coef_loss, optimizer=Adam(lr=1e-3),
    #                     metrics=[dice_coef, 'accuracy', precision, recall, f1score])
    # autoencoder.summary()

    return autoencoder
