import os
import sys
import warnings

import cv2
import numpy as np
from keras.preprocessing.image import ImageDataGenerator
from tqdm import tqdm

# Set some parameters
IMG_WIDTH = 256
IMG_HEIGHT = 256
IMG_CHANNELS = 3
TRAIN_PATH = 'data/train/'
TEST_PATH = 'data/test/'

TRAIN_ERROR_IDS = [
    # '7b38c9173ebe69b4c6ba7e703c0c27f39305d9b2910f46405993d2ea7a963b80',
    # 'b1eb0123fe2d8c825694b193efb7b923d95effac9558ee4eaf3116374c2c94fe',
    # '9bb6e39d5f4415bc7554842ee5d1280403a602f2ba56122b87f453a62d37c06e',
    # '1f0008060150b5b93084ae2e4dabd160ab80a95ce8071a321b80ec4e33b58aca',
    '58c593bcb98386e7fd42a1d34e291db93477624b164e83ab2afa3caa90d1d921', #fali cosak
]


warnings.filterwarnings('ignore', category=UserWarning, module='skimage')

def create_train_data():
    # Get and resize train images and masks
    train_ids = next(os.walk(TRAIN_PATH))[1]
    X_train = np.zeros((len(train_ids), IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS), dtype=np.uint8)
    Y_train = np.zeros((len(train_ids), IMG_HEIGHT, IMG_WIDTH, 1), dtype=np.bool)
    print('Getting and resizing train images and masks ... ')
    sys.stdout.flush()
    for n, id_ in tqdm(enumerate(train_ids), total=len(train_ids)):
        if id_ in TRAIN_ERROR_IDS:
            print('Skipping ID due to bad training data:', id_)
            continue
        path = TRAIN_PATH + id_
        img = cv2.imread(path + '/images/' + id_ + '.png')
        # img = cv2.cvtColor(img, cv2.COLOR_BGR2LAB)
        img = cv2.resize(img, (IMG_HEIGHT, IMG_WIDTH))
        X_train[n] = img

        mask = np.zeros((IMG_HEIGHT, IMG_WIDTH, 1), dtype=np.bool)
        for mask_file in next(os.walk(path + '/masks/'))[2]:
            mask_ = cv2.imread(path + '/masks/' + mask_file, 0)
            mask_ = np.expand_dims(cv2.resize(mask_, (IMG_HEIGHT, IMG_WIDTH)), axis=-1)
            mask = np.maximum(mask, mask_)
        Y_train[n] = mask

    np.save('./data/imgs_train.npy', X_train)
    np.save('./data/imgs_mask_train.npy', Y_train)
    print('Saving to .npy files done.')
    return X_train, Y_train

def load_train_data():
    base = os.path.join(os.path.dirname(__file__), 'data/')
    imgs_train = np.load(base + 'imgs_train.npy')
    imgs_mask_train = np.load(base + 'imgs_mask_train.npy')
    return imgs_train, imgs_mask_train

def create_test_data():
    # Get and resize test images
    test_ids = next(os.walk(TEST_PATH))[1]
    X_test = np.zeros((len(test_ids), IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS), dtype=np.uint8)
    sizes_test = []
    print('Getting and resizing test images ... ')
    sys.stdout.flush()
    for n, id_ in tqdm(enumerate(test_ids), total=len(test_ids)):
        path = TEST_PATH + id_
        img = cv2.imread(path + '/images/' + id_ + '.png')
        # img = cv2.cvtColor(img, cv2.COLOR_BGR2LAB)
        sizes_test.append([img.shape[0], img.shape[1]])
        img =cv2.resize(img, (IMG_HEIGHT, IMG_WIDTH))
        X_test[n] = img

    np.save('./data/imgs_test.npy', X_test)
    np.save('./data/sizes_test.npy', sizes_test)
    np.save('./data/test_ids.npy', test_ids)
    print('Saving to .npy files done.')
    return X_test, sizes_test, test_ids

def load_test_data():
    base = os.path.join(os.path.dirname(__file__), 'data/')
    imgs_test = np.load(base + 'imgs_test.npy')
    sizes_test = np.load(base + 'sizes_test.npy')
    test_ids = np.load(base + 'test_ids.npy')
    return imgs_test, sizes_test, test_ids

def generator(xtr, xval, ytr, yval, batch_size):
    data_gen_args = dict(horizontal_flip=True,
                         vertical_flip=True,
                         rotation_range=90.,
                         width_shift_range=0.1,
                         height_shift_range=0.1,
                         zoom_range=0.1)
    image_datagen = ImageDataGenerator(**data_gen_args)
    mask_datagen = ImageDataGenerator(**data_gen_args)
    image_datagen.fit(xtr, seed=7)
    mask_datagen.fit(ytr, seed=7)
    image_generator = image_datagen.flow(xtr, batch_size=batch_size, seed=7)
    mask_generator = mask_datagen.flow(ytr, batch_size=batch_size, seed=7)
    train_generator = zip(image_generator, mask_generator)

    val_gen_args = dict()
    image_datagen_val = ImageDataGenerator(**val_gen_args)
    mask_datagen_val = ImageDataGenerator(**val_gen_args)
    image_datagen_val.fit(xval, seed=7)
    mask_datagen_val.fit(yval, seed=7)
    image_generator_val = image_datagen_val.flow(xval, batch_size=batch_size, seed=7)
    mask_generator_val = mask_datagen_val.flow(yval, batch_size=batch_size, seed=7)
    val_generator = zip(image_generator_val, mask_generator_val)

    return train_generator, val_generator


if __name__ == '__main__':
    create_train_data()
    create_test_data()
    print('Done!')
