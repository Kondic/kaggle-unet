def get_unet_model(IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS):
    # Build U-Net model
    inputs = Input((IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS))
    s = Lambda(lambda x: x / 255.)(inputs)

    conv1 = conv_3x3(BASE, s, activation, kernel_init, drop_rate=0.1)
    conv1 = conv_3x3(BASE, conv1, activation, kernel_init)
    pool1 = POOLING_FUNCTION(pool_size=(2, 2))(conv1)

    conv2 = conv_3x3(2*BASE, pool1, activation, kernel_init, drop_rate=0.1)
    conv2 = conv_3x3(2*BASE, conv2, activation, kernel_init)
    pool2 = POOLING_FUNCTION(pool_size=(2, 2))(conv2)

    conv3 = conv_3x3(4*BASE, pool2, activation, kernel_init, drop_rate=0.3)
    conv3 = conv_3x3(4*BASE, conv3, activation, kernel_init)
    pool3 = POOLING_FUNCTION(pool_size=(2, 2))(conv3)

    conv4 = conv_3x3(8*BASE, pool3, activation, kernel_init, drop_rate=0.2)
    conv4 = conv_3x3(8*BASE, conv4, activation, kernel_init)
    pool4 = POOLING_FUNCTION(pool_size=(2, 2))(conv4)

    conv5 = conv_3x3(16*BASE, pool4, activation, kernel_init,drop_rate=0.3)
    conv5 = conv_3x3(16*BASE, conv5, activation, kernel_init)

    up6 = concatenate2x2(8*BASE,conv5, conv4)
    conv6 = conv_3x3(8*BASE, up6, activation, kernel_init,drop_rate=0.2)
    conv6 = conv_3x3(8*BASE, conv6, activation, kernel_init)

    up7 = concatenate2x2(4*BASE,conv6, conv3)
    conv7 = conv_3x3(4*BASE, up7, activation, kernel_init, drop_rate=0.2)
    conv7 = conv_3x3(4*BASE, conv7, activation, kernel_init)

    up8 = concatenate2x2(2*BASE,conv7, conv2)
    conv8 = conv_3x3(2*BASE, up8, activation, kernel_init, drop_rate=0.1)
    conv8 = conv_3x3(2*BASE, conv8, activation, kernel_init)

    up9 = concatenate2x2(BASE,conv8, conv1)
    conv9 = conv_3x3(BASE, up9, activation, kernel_init, drop_rate=0.1)
    conv9 = conv_3x3(BASE, conv9, activation, kernel_init)

    outputs = Conv2D(1, (1, 1), activation='sigmoid')(conv9)

    model = Model(inputs=[inputs], outputs=[outputs])

    return model