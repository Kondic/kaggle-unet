import cv2
import numpy as np
import pandas as pd
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
from keras.models import load_model
from keras.optimizers import Nadam
from sklearn.model_selection import train_test_split

from data_processing import load_train_data, load_test_data, generator, IMG_HEIGHT, IMG_WIDTH
from models.unet_model import get_unet_model
from models.segnet import get_segnet
from utility.data_augmentation import img_to_LAB_normalized, augment_data_normalized
from utility.metrics import mean_iou, bce_dice_loss
from utility.utils import prob_to_rles, plot_training, show_images

# Set some parameters
use_generator = False
batch_size = 16
nb_epochs = 100
split_ratio = 0.1


X_train, Y_train = load_train_data()
X_test, sizes_test, test_ids = load_test_data()

X_train, Y_train = augment_data_normalized(X_train, Y_train)
# X_train = np.array([np.expand_dims(clahe_justl_normalization(img), axis=-1) for img in X_train])
X_test = np.array([np.expand_dims(img_to_LAB_normalized(img), axis=-1) for img in X_test])

X_train = X_train.astype('float32')
mean = np.mean(X_train)  # mean for data centering
std = np.std(X_train)  # std for data normalization
X_train -= mean
X_train /= std
# #
X_test = X_test.astype('float32')
X_test -= mean
X_test /= std

x_train, x_val, y_train, y_val = train_test_split(X_train, Y_train, test_size=split_ratio)
if use_generator:
    train_generator, val_generator = generator(x_train, x_val, y_train, y_val, batch_size)

print('Data loaded and transformed!')
def train():
    # model = get_unet_model(IMG_HEIGHT, IMG_WIDTH, 1)
    model = get_segnet(IMG_WIDTH, IMG_HEIGHT)
    # model = load_model('model.h5', custom_objects={'mean_iou': mean_iou, 'bce_dice_loss': bce_dice_loss})
    model.compile(optimizer=Nadam(lr=0.001), loss=bce_dice_loss, metrics=[mean_iou])
    model.summary()
    # Fit model
    earlystopper = EarlyStopping(patience=5, verbose=2)
    reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.1,
                                  patience=3, min_lr=1e-6)
    checkpointer = ModelCheckpoint('model.h5', verbose=2, save_best_only=True)
    print('Training!')
    if not use_generator:
        results = model.fit(x_train, y_train, batch_size=batch_size, validation_data=[x_val, y_val], epochs=nb_epochs,
                            callbacks=[earlystopper, checkpointer, reduce_lr], verbose=1)

    if use_generator:
        results = model.fit_generator(train_generator, verbose=1, steps_per_epoch=len(x_train) / batch_size, epochs=nb_epochs,
                                      validation_data=val_generator, validation_steps=len(x_val) / batch_size,
                                      callbacks=[earlystopper, checkpointer, reduce_lr])

    plot_training(results)

def submission(model):
    if model is None:
        model = load_model('model.h5', custom_objects={'mean_iou': mean_iou, 'bce_dice_loss': bce_dice_loss})

    print('Predicting results....')
    preds_test = model.predict(X_test, verbose=0)
    # Create list of upsampled test masks
    preds_test_upsampled = []
    for i in range(len(preds_test)):
        preds_test_upsampled.append(cv2.resize(preds_test[i],
                                               (sizes_test[i][1], sizes_test[i][0])))

    new_test_ids = []
    rles = []
    for n, id_ in enumerate(test_ids):
        rle = list(prob_to_rles(preds_test_upsampled[n]))
        rles.extend(rle)
        new_test_ids.extend([id_] * len(rle))

    # Create submission DataFrame
    print('Saving results .......')
    sub = pd.DataFrame()
    sub['ImageId'] = new_test_ids
    sub['EncodedPixels'] = pd.Series(rles).apply(lambda x: ' '.join(str(y) for y in x))
    sub.to_csv('submission.csv', index=False)

def sanity_check(model=None):
    print('Sanity check...')
    if model is None:
        model = load_model('model.h5', custom_objects={'mean_iou': mean_iou, 'bce_dice_loss': bce_dice_loss})
    # preds_train = model.predict(x_train, verbose=0)
    preds_val = model.predict(x_val, verbose=0)
    preds_test = model.predict(X_test, verbose=0)
    # Threshold predictions
    # preds_train_t = (preds_train > 0.5).astype(np.uint8)
    preds_val_t = (preds_val > 0.5).astype(np.uint8)
    preds_test_t = (preds_test > 0.5).astype(np.uint8)


    # for ix in np.random.randint(0, len(preds_train_t), 5):
    #     show_images([np.squeeze(x_train[ix]),
    #                  np.squeeze(y_train[ix]),
    #                  np.squeeze(preds_train_t[ix])],
    #                  titles=["X", "Y", "Predicted"])
    for ix in np.random.randint(0, len(preds_test_t), 5):
        show_images([
                 np.squeeze(x_val[ix]),
                 np.squeeze(y_val[ix]),
                 np.squeeze(preds_val_t[ix])], titles=["X", "Y", "Predicted"])

    for i in np.random.randint(0, len(preds_test_t), 10):
        show_images([np.squeeze(X_test[i]), np.squeeze(preds_test_t[i])], titles=["X", "Predicted"])


if __name__ =='__main__':
    model = None
    model = train()
    sanity_check(model)
    submission(model)