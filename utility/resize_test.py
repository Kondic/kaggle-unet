import os
import sys
import warnings

import cv2
import numpy as np
from tqdm import tqdm

from utility.utils import show_images

# Set some parameters
IMG_WIDTH = 256
IMG_HEIGHT = 256
IMG_CHANNELS = 3
TRAIN_PATH = 'data/train/'
TEST_PATH = 'data/test/'

warnings.filterwarnings('ignore', category=UserWarning, module='skimage')

def create_train_data(grey=False, data_augmentation=False):
    # Get and resize train images and masks
    train_ids = next(os.walk(TRAIN_PATH))[1]
    print('Getting and resizing train images and masks ... ')
    sys.stdout.flush()
    for n, id_ in tqdm(enumerate(train_ids), total=len(train_ids)):
        path = TRAIN_PATH + id_

        img = cv2.imread(path + '/images/' + id_ + '.png')
        img_resized = cv2.resize(img, (IMG_HEIGHT, IMG_WIDTH), interpolation=cv2.INTER_AREA)

            # X_train[data_len + n] = gamma1_img
            # X_train[2 * data_len + n] = gamma2_img
        if img.shape[0] == IMG_WIDTH:
            continue
        mask = np.zeros((img.shape[0], img.shape[1]), dtype=np.bool)
        for mask_file in next(os.walk(path + '/masks/'))[2]:
            mask_ = cv2.imread(path + '/masks/' + mask_file, 0)
            mask = np.maximum(mask, mask_)
            # mask = np.squeeze()
        mask_resized = cv2.resize(mask, (IMG_HEIGHT, IMG_WIDTH), interpolation=cv2.INTER_AREA)
        mask_resized = mask_resized > 0.5
            # mask = np.maximum(mask, mask_)
        show_images([img, img_resized, mask, mask_resized],cols=2)

create_train_data()