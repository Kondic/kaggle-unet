import warnings

import numpy as np
from sklearn.model_selection import train_test_split

from data_processing import load_train_data, load_test_data, generator
from utility.utils import show_images


# Set some parameters
use_generator = True
batch_size = 10
nb_epochs = 100
split_ratio = 0.1

warnings.filterwarnings('ignore', category=UserWarning, module='skimage')


X_train, Y_train = load_train_data()
X_test, _, _ = load_test_data()

x_train, x_test, y_train, y_test = train_test_split(X_train, Y_train, test_size=split_ratio)
if use_generator:
    train_generator, val_generator = generator(x_train, x_test, y_train, y_test, batch_size)
if use_generator:
    for x,y in train_generator:
        for index in range(0, batch_size):
            show_images([x[index], np.squeeze(y[index])], titles=["X", "Mask"])
            # pass

if not use_generator:
    # for image in X_test:
    #     show_images([image])
    for x, y in zip(X_train, Y_train):
        show_images([x, np.squeeze(y)], titles=["X", "Mask"])