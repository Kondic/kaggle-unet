import cv2
import numpy as np

def adjust_gamma(image, gamma=1.0):
    # build a lookup table mapping the pixel values [0, 255] to
    # their adjusted gamma values
    invGamma = 1.0 / gamma
    table = np.array([((i / 255.0) ** invGamma) * 255
                      for i in np.arange(0, 256)]).astype("uint8")

    # apply gamma correction using the lookup table
    return cv2.LUT(image, table)

def adjust_clahe(image):
    """
    CLAHE for color images
    :param image:
    :return:
    """
    lab = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)

    lab_planes = cv2.split(lab)
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(4, 4))
    lab_planes[0] = clahe.apply(lab_planes[0])
    lab = cv2.merge(lab_planes)
    # return lab
    return cv2.cvtColor(lab, cv2.COLOR_LAB2BGR)

def clahe_justl_normalization(image):
    """
    Convert the imagte to LAB color system. Apply CLAHE on L channel
    and invert image in order to have consistent dataset.
    :param image:
    :return: Transformed L chanel
    """
    lab = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
    img = clahe.apply(lab[:,:,0])

    img = 255 - img if img.mean() > 127 else img
    return img

def adjust_gamma_normalization(image, gamma):
    lab = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
    img = adjust_gamma(lab[:,:,0],gamma)
    img = 255 - img if img.mean() > 127 else img

    return np.expand_dims(img, axis=-1)

def get_gamma_clahe(image):
    adjusted_clahe = adjust_clahe(image)
    gamma1 = 0.9
    gamma2 = 2.0

    gamma1_img = adjust_gamma(image, gamma=gamma1)
    gamma2_img = adjust_gamma(image, gamma=gamma2)

    return [image, gamma1_img, gamma2_img, adjusted_clahe]

def get_gamma_clahe_normalized(image):
    adjusted_clahe = np.expand_dims(clahe_justl_normalization(image), axis=-1)
    gamma1 = 0.9
    gamma2 = 2.0

    gamma1_img = adjust_gamma_normalization(image, gamma=gamma1)
    gamma2_img = adjust_gamma_normalization(image, gamma=gamma2)

    image = np.expand_dims(img_to_LAB_normalized(image), axis=-1)
    return [image, gamma1_img, gamma2_img, adjusted_clahe]

def augment_data(X, Y):
    new_X = np.array([get_gamma_clahe(img) for img in X])
    new_Y = np.array([[mask, mask, mask, mask] for mask in Y])

    new_X = new_X.reshape((4*X.shape[0], X.shape[1], X.shape[2], X.shape[3]))
    new_Y = new_Y.reshape((4*Y.shape[0], Y.shape[1], Y.shape[2], Y.shape[3]))
    return new_X, new_Y

def augment_data_normalized(X, Y):
    new_X = np.array([get_gamma_clahe_normalized(img) for img in X])
    new_Y = np.array([[mask, mask, mask, mask] for mask in Y])

    new_X = new_X.reshape((4*X.shape[0], X.shape[1], X.shape[2], 1))
    new_Y = new_Y.reshape((4*Y.shape[0], Y.shape[1], Y.shape[2], Y.shape[3]))
    return new_X, new_Y

def img_to_LAB_normalized(image):
    lab =  cv2.cvtColor(image, cv2.COLOR_BGR2LAB)
    return lab[:,:,0] if lab[:,:,0].mean() < 127 else 255 - lab[:,:,0]
if __name__ == '__main__':
    from data_processing import load_train_data
    from utility.utils import show_images
    import os


    print(os.path.join(os.path.dirname(__file__), 'data/'))
    X, Y = load_train_data()
    #
    # augment_data(X,Y)
    for i in np.random.randint(0, len(X), 20):
        original = X[i]
        # img = clahe_justl_normalization(X[i])
        # lab =img_to_LAB_normalized(original)
        # hsv = cv2.cvtColor(original, cv2.COLOR_BGR2HSV)
        # hsl = cv2.cvtColor(original, cv2.COLOR_BGR2HLS)
        # show_images([original, lab, hsv[:,:,2],hsv[:,:,0], hsl[:,:,1], hsl[:,:,2], np.squeeze(Y[i])])
        org, gamma1_img, gamma2_img, adjusted_clahe = get_gamma_clahe_normalized(original)
        show_images([np.squeeze(org), np.squeeze(gamma1_img), np.squeeze(gamma2_img), np.squeeze(adjusted_clahe), np.squeeze(Y[i])], 2)
        # cv2.imshow("Images", np.hstack([original, gamma1_img, gamma2_img, adjusted_clahe]))
        # cv2.waitKey(0)